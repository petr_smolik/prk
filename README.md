
# Operace
soucin *

soucet +

zavorky ()

modulo %

porovnání <>=

one_row_if ()?:

# Priorita
()?:

<>=

()

\+

\*

%

# Datove typy

### bool

povolené hodnoty: true, false

### double

povolené hodnoty: [-.0123456789]*

### string

povolené hodnoty: '[a-zA-Z0-9]*'

# Správný výraz

'ahoj'

(((-88%7)))

-5.1*0.65

(2.23%1)+21<12*2.23+(1+21*12)

-88%7

true

{true}?'hello':0.006

{(2.23%1)+21<12*2.23%(1+21*12)}?'hello':0.006

# Chybný výraz

ahoj

'dobry_den'

(((-88%7))

+5.1*0.65

-5.1++0.65

-88+7%

(2.23%1)+21<12*2.2==(1+21*12)

True

{pomelo<okurka}?'hello':0.006

{(2.23%1)+21<12*2.23%(1+21*12)}'hello':0.006

# Gramatika

oneRowIf = "{", (bool | compLogic ), "}?", result , ":", result;

result = bool | double | string;

compLogic = arithLogic, [compareOperator, arithLogic];

arithLogic = arithLogic1 | arithLogic2;

arithLogic2 = "(",  (arithLogic1 | arithLogic2), ")",  [operator, arithLogic];

arithLogic1 = double, [operator, arithLogic];

operator = "+" | "*" | "%";

compareOperator = ">" | "<" | "==";

bool = "true" | "false";

double = ["-"], (double1  | double2);

double1 = nonz, {digit}, [".", {digit}];

double2 = zero, ".", {digit};

string = "'", {digit | letter}, "'";

nonz="1"|"2"|"3"|"4"|"5"|"6"|"7"|"8"|"9";

digit= zero  | nonz;

zero = "0";

letter = "a"|"b"|"c"|"d"|"e"|"f"|"g"|"h"|"i"|"j"|"k"|"l"|"m"|"n"|"o"|"p"|"q"|"r"|"s"|"t"|"u"|"v"|"w"|"x"|"y"|"z"|"A"|"B"|"C"|"D"|"E"|"F"|"G"|"H"|"I"|"J"|"K"|"L"|"M"|"N"|"O"|"P"|"Q"|"R"|"S"|"T"|"U"|"V"|"W"|"X"|"Y"|"Z";

syntax = oneRowIf | compLogic | bool | string;
