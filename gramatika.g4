
grammar Expr;	

syntax: oneRowIf | compLogic | BOOL | STRING;

oneRowIf: '{' (BOOL | compLogic) '}?' oriResult ':' oriResult;
oriResult: BOOL | DOUBLE | STRING;

compLogic: arithLogic (COMPAREOPERATOR arithLogic)?;
arithLogic: ('(' arithLogic ')') | DOUBLE (OPERATOR arithLogic)*;


OPERATOR: '+' | '*' | '%';
COMPAREOPERATOR: '>' | '<' | '==';

DOUBLE: ('-')? ((NONZ DIGIT* ('.' DIGIT*)?) | (ZERO '.' DIGIT*));
STRING: '\'' (DIGIT | LETTER)* '\'';

DIGIT: ZERO | NONZ;
NONZ: [1-9];
ZERO: '0';
LETTER: [a-zA-Z];
BOOL: "true" | "false";

WS: [ \t\r\n]+ -> skip ; // skip spaces, tabs, newlines
